
from datetime import timedelta

if __name__ == "__main__":
    from datetime import datetime
    now = datetime.now()
    print(now)
    # 2023天后
    print(now + timedelta(days=2024))
    # 从1970.1.1 00:00:00开始计算，到现在经过了多少秒
    print(now.timestamp())
    # 将时间戳转换为时间
    print(datetime.fromtimestamp(now.timestamp()))
    print(10 ** 2)
