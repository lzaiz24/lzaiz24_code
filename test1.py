# for n in range(100, 999):
#     bai = n // 100
#     shi = n % 100 // 10
#     ge = n % 10
#     if bai ** 3 + shi ** 3 + ge ** 3 == n:
#         print(n)

# print()
# for m in range(100, 999):
#     st = str(m)
#     i = int(st[0])
#     j = int(st[1])
#     k = int(st[2])
#     if i**3 + j**3 + k**3 == m:
#         print(m)

# num = int(input("num ="))
# for n in range(2, num):
#     flag = 1
#     if num % n == 0:
#         flag = 0
#         break
# if flag == 1:
#     print("prime")


# def fib(n):
#     a, b = 0, 1
#     while a < n:
#         print(a, end=',')
#         a, b = b, a+b
#     print()


# f = fib
# f(2000)


# def fib2(n):  # return Fibonacci series up to n
#     """Return a list containing the Fibonacci series up to n."""
#     result = []
#     a, b = 0, 1
#     while a < n:
#         result.append(a)    # see below
#         a, b = b, a+b
#     print(result)
#     return result


# f100 = fib2(100)    # call it
# f100                # write the result


# arr = [1, 2, 3, 4, 5]
# if 6 in arr:
#     print("ok")
# else:
#     print("no")


# arr = [1]
# print(arr)
# arr.append(2)
# print(arr)
# arr.insert(1, 3)
# print(arr)
# arr.remove(3)
# print(arr)
# arr.reverse()
# print(arr)
# print(arr.count(1))
# arr = [23, 54, 13, 52, 12]
# arr.sort()
# print(arr)

# stack = [3, 4, 5]
# stack.append(6)
# stack.append(7)
# print(stack)
# print(stack.pop())
# print(stack.pop())
# print(stack.pop())
# print(stack)

# # from collections import deque
# queue = deque(["Eric", "John", "Michael"])
# queue.append("Terry")
# queue.append("Graham")
# print(queue.popleft())
# print(queue.popleft())
# print(queue)

# # 列表推导式
# squares = []
# for x in range(10):
#     squares.append(x**2)
# print(squares)
# squares = list(map(lambda x: x**2, range(10)))
# print(squares)
# arr = [x**2 for x in range(10)]
# print(arr)

# print([(x, y) for x in [1, 2, 3] for y in [3, 1, 4] if x != y])

# t = 12345, 54321, "Hello!"
# print(t[0])
# print(t)

# # 集合

# basket = {'apple', 'orange', 'apple', 'pear', 'orange', 'banana'}
# print(basket)
# print('orange' in basket)

# a = set('abracadabra')
# b = set('alacazam')
# print(a)
# print(a - b)
# print(a | b)
# print(a & b)
# print(a ^ b)


# a = {x for x in 'aknagkjnagcb' if x not in 'abc'}
# print(a)


# 字典

# tel = {'jack': 4098, 'sape': 4139}
# tel['guido'] = 4127
# print(tel)
# print(tel['jack'])
# del tel['sape']
# print(tel)
# tel['irv'] = 4127
# print(list(tel))
# print(sorted(tel))
# print('guido' in tel)
# print('jack' not in tel)

# arr1 = dict([('sape', 4139), ('guido', 4127), ('jack', 4098)])
# print(arr1)
# arr2 = {x: x**2 for x in (2, 4, 6)}
# print(arr2)
# arr3 = dict(a=1, b=2, c=3)
# print(arr3)


# knights = {'gallahad': 'the pure', 'robin': 'the brave'}
# for k, v in knights.items():
#     print(k, v)

# for i, v in enumerate(['tic', 'tac', 'toe']):
#     print(i, v)


# dic = {x: y for x in range(10) for y in range(5) if x != y}
# for i, j in dic.items():
#     print(i, j)


# dic2 = {0:1, 0:2}
# print(dic2)



