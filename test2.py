# import json
# year = 1234
# event = 'abc'
# print(f'results in {year} {event}')

# yes = 42_4124_124
# no = 42_4124_425
# percentage = yes / (yes+no)
# print('{:-9} YES votes {:2.2%}'.format(yes, percentage))


# json.dumps([1, 'simple', 'list'])

# with open("a.json", 'r') as f:
#         info = json.loads(f.read())
#     for k, v in info.items():
#         print(k, v)

# f = open("a.json", "w")
# info = {
#     "1923": "c++",
#     "1222": "python",
#     "123": (1, 2, 3),
# }

# json.dump(info, f)
# f.close()
# f = open("b.json",'w')
#     info = {x : x**2 for x in range(1, 11)}
#     json.dump(info, f)
#     f.close()
# with open("b.json",'r') as f:
#         info = json.loads(f.read())
#     for k,v in info.items():
#         print(k,v)

# class MyComplex:
#         def __init__(self, realpart, imagpart):
#             self.r = realpart
#             self.i = imagpart
#         def __func__(self):
#             print("hello world")
#     x = MyComplex(3.0, -4.5)
#     x.__func__()
#     print(x.r, x.i)
#     class DerivedClassName(MyComplex):
#         def __init__(self, realpart, imagpart):
#             self.r = realpart
#             self.i = imagpart
#         def __func__(self):
#             print("hello world")
#     y = DerivedClassName(123, 3123)
#     y.__func__()

# import os
# if __name__ == "__main__":
#     os.system("start https://www.baidu.com")



# from datetime import timedelta


# if __name__ == "__main__":
#     from datetime import datetime
#     now = datetime.now()
#     print(now)
#     # 2023天后
#     print(now + timedelta(days=2023))
#     # 从1970.1.1 00:00:00开始计算，到现在经过了多少秒
#     print(now.timestamp())
#     # 将时间戳转换为时间
#     print(datetime.fromtimestamp(now.timestamp()))
